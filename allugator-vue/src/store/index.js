import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'
// components
import login from '@/components/auth/store'
import produtos from '@/components/produtos/store'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    login,
    produtos
  },
  plugins: [
    createPersistedState({
      key: 'allugatorKey',
      // localStorage
      getState: (key) => {
        return JSON.parse(localStorage.getItem(key));
      },
      setState: (key, state) => {
          //console.log('setState', JSON.parse(localStorage.getItem(key)));
          localStorage.setItem(key, JSON.stringify(state));
      },
    })
  ]
});
