import Vue from 'vue'
import Router from 'vue-router'
import TheHome from '@/components/the-home'
import Produtos from '@/components/produtos/produtos'
import Login from '@/components/auth/login'
import EditarProduto from '@/components/produtos/editar-produto'
import CadastrarProduto from '@/components/produtos/cadastrar-produto'
import MeusProdutos from '@/components/produtos/meus-produtos'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '*',
      redirect: '/'
    },
    {
      path: '/',
      name: 'home',
      component: TheHome
    },
    {
      path: '/produtos',
      name: 'produtos',
      component: Produtos
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/produto/:id',
      name: 'produto_editar',
      component: EditarProduto
    },
    {
      path: '/produto',
      name: 'produto_cadastrar',
      component: CadastrarProduto
    },
    {
      path: '/meus-produtos',
      name: 'meus_produtos',
      component: MeusProdutos
    }
  ]
})
