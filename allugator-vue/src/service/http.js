import Vue from 'vue'
import axios from 'axios'
import store from '@/store'
var base64 = require('base-64');


const axiosConfig = {
  baseURL: process.env.API_URL,
  timeout: 30000,
};

const http = axios.create(axiosConfig);
http.interceptors.request.use(function (request) {

  request.headers = request.headers || {};
  if (store.state.login.isUsuarioLogado) {
    // may also use sessionStorage
    request.headers.Authorization = "Basic " + base64.encode(store.state.login.usuarioLogado.login +':'+store.state.login.usuarioLogado.senha);
  }

  return request;
}, function (rejection) {

  //return Promise.reject(rejection);
  return error.response
});

// Add a response interceptor
http.interceptors.response.use(function (response) {

  return response;
}, function (error) {

  //return Promise.reject(rejection);
  return error.response
});

export default http;
