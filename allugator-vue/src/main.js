// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import ElementUI from 'element-ui'
// jQuery
window.$ = window.jQuery = require('jquery');
// Bootstrap
import 'bootstrap/dist/js/bootstrap.min.js'
// toastr
import VueToastr from '@deveodk/vue-toastr'
// Vue tables 2
import {ServerTable, ClientTable, Event} from 'vue-tables-2';
// ES6 Modules or TypeScript
import swal from 'sweetalert2'
global.swal = swal;
// vue mask
import VueTheMask from 'vue-the-mask'
// v money
import money from 'v-money'

Vue.use(ElementUI)
Vue.use(VueToastr, {
  defaultPosition: 'toast-top-right',
  defaultType: 'info',
  defaultTimeout: 3000
})
Vue.use(ClientTable);
Vue.use(VueTheMask)
Vue.use(money)

Vue.config.productionTip = false

// vuex
import store from '@/store'
// axios
import http from '@/service/http'
global.http = http;

router.beforeEach((to, from, next) => {

  if(localStorage.getItem('token')) {
    next()
  } else if(to.name === "produto_editar" || to.name === "produto_cadastrar" || to.name === "meus_produtos") {
    next('/login');
  }

  next();
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
