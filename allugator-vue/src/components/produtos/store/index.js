const state = {
  allprodutos: [],
}

const actions = {
  setAllProdutos ({commit}, val) {
      commit('SET_ALL_PRODUTOS', val);
  },
}

const mutations = {
  SET_ALL_PRODUTOS (state, val) {
      state.allprodutos = val;
  },
}

const getters = {
  getAllProdutos: (state) => state.allprodutos,
}

export default {
  state,
  actions,
  mutations,
  getters
}
