import { mapGetters } from 'vuex'

export default {
  data () {
    return {
      tableData: [],
    }
  },
  computed: {
    ...mapGetters({
      isUsuarioLogado: 'getIsUsuarioLogado',
      usuarioLogado: 'getUsuarioLogado',
      allprodutos: 'getAllProdutos'
    })
  },
  methods: {
    async getDataOwner() {
      const response = await http.get('/produtos')
      const dados = response.data
      if(response.status === 200) {
        console.log(dados._embedded.produtos);
        this.tableData = dados._embedded.produtos.filter((v) => {
          return v.owner === this.usuarioLogado.id
        });
        this.$store.dispatch('setAllProdutos', this.tableData);
        return Promise.resolve(response);
      }
    },
    async getDataNotOwner() {
      const response = await http.get('/produtos')
      const dados = response.data
      if(response.status === 200) {
        console.log(dados._embedded.produtos);
        this.tableData = dados._embedded.produtos.filter((v) => {
          return v.owner !== this.usuarioLogado.id
        });
        this.$store.dispatch('setAllProdutos', this.tableData);
        return Promise.resolve(response);
      }
    },
  }
}
